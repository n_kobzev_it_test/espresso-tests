You can run Espresso tests from Android Studio or from the command-line. Make sure to specify AndroidJUnitRunner as the default instrumentation runner in your project.(Android Google Docs)

Open Android Studio -> Open project-> Open (com.example.dario.myapplication.MainActivityTest).

To the left of the class name will appear the icon "Play", click and run the test in the debug or release mode. Enjoy.

![Scheme](images/screen1.jpg)
