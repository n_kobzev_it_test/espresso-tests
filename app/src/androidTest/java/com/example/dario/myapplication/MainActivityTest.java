package com.example.dario.myapplication;

import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.swipeDown;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayingAtLeast;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.assertion.ViewAssertions.matches;

public class MainActivityTest {



    @Rule
    public ActivityTestRule<MainActivity> activityTestRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void swipeRefresh(){
        onView(withId(R.id.swipeRefresh)) .perform(withCustomConstraints(swipeDown(), isDisplayingAtLeast(85)));
    }

    @Test
    public void scrollRecyclerView() {
        RecyclerView recyclerView = activityTestRule.getActivity().findViewById(R.id.recyclerView);
        int itemCount = recyclerView.getAdapter().getItemCount();
        onView(withId(R.id.recyclerView)).check(new RecyclerViewItemCountAssertion(itemCount));
        for(int i = 0; i < itemCount; i++) {
            onView(withId(R.id.recyclerView))
                    .perform(RecyclerViewActions.scrollToPosition(i));
        }

    }

    @Test
    public void newsOpen() {
        RecyclerView recyclerView = activityTestRule.getActivity().findViewById(R.id.recyclerView);
        int itemCount = recyclerView.getAdapter().getItemCount();
        onView(withId(R.id.recyclerView)).check(new RecyclerViewItemCountAssertion(itemCount));
        for(int i = 0; i < itemCount; i++) {
            clickRecyclerItem(i);
            pressBack();
        }
    }

    private void clickRecyclerItem(int position){
        onView(withId(R.id.recyclerView)).perform(
                RecyclerViewActions.actionOnItemAtPosition(
                        position,
                        new ViewAction() {
                            @Override
                            public Matcher<View> getConstraints() {
                                return null;
                            }

                            @Override
                            public String getDescription() {
                                return "Click on specific button";
                            }

                            @Override
                            public void perform(UiController uiController, View view) {
                                View button = view.findViewById(R.id.buttonReadNews);

                                button.performClick();

                            }
                        })
        );
        onView(withId(R.id.textTitle)).check(matches(isDisplayed()));
        onView(withId(R.id.textDate)).check(matches(isDisplayed()));
        onView(withId(R.id.textMsg)).check(matches(isDisplayed()));
    }

    private void pressBack() {
        onView(isRoot()).perform(ViewActions.pressBack());
    }

    public static ViewAction withCustomConstraints(final ViewAction action, final Matcher<View> constraints) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return constraints;
            }
            @Override
            public String getDescription() {
                return action.getDescription();
            }
            @Override
            public void perform(UiController uiController, View view) {
                action.perform(uiController, view);
            }
        };
    }
}