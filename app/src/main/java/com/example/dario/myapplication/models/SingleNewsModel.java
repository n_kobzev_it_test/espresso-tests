package com.example.dario.myapplication.models;

public class SingleNewsModel {
    public int id;
    public String title;
    public String text;
    public String annonceDate;
    public String description;
    public String newsType;
}

