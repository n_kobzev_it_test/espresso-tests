package com.example.dario.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.dario.myapplication.models.SingleNewsModel;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    private SwipeRefreshLayout mSwipeLayout;
    private RecyclerView recyclerView;
    private List<SingleNewsModel> newsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        String date = dateFormat.format(calendar.getTime());

        newsList = new ArrayList<>();

        for(int i = 0; i < 20; i++) {
            SingleNewsModel news  = new SingleNewsModel();
            news.id = i;
            news.newsType = "news";
            news.annonceDate = parseDate(date);
            news.title = "Title " + i;
            news.description = "Description " + i;
            news.text = "Text " + i;
            newsList.add(news);
        }

        recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        NewsAdapter adapter = new NewsAdapter(newsList, this);
        recyclerView.setAdapter(adapter);



        mSwipeLayout = findViewById(R.id.swipeRefresh);
        mSwipeLayout.setOnRefreshListener(this);
    }

    private String parseDate(String date){
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
        LocalDate curDate = LocalDate.parse(date, inputFormatter);
        String[] mounths = getResources().getStringArray(R.array.months_array);
        int num = curDate.getMonthValue() - 1;
        return curDate.getDayOfMonth() + " " + mounths[num] + " " + curDate.getYear();
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                mSwipeLayout.setRefreshing(false);//убрать в релизе
            }
        }, 1000);

    }
}
