package com.example.dario.myapplication;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.dario.myapplication.models.SingleNewsModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

public class NewsAdapter extends Adapter<NewsAdapter.ViewHolder> {
    private List<SingleNewsModel> newsList;
    private Context mainCtx;

    public NewsAdapter(List<SingleNewsModel> newsList, Context mainCtx){

        this.newsList = newsList;
        this.mainCtx = mainCtx;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.news_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        final SingleNewsModel singleNews = newsList.get(i);

        viewHolder.textTitle.setText(singleNews.title);
        viewHolder.textDate.setText(singleNews.annonceDate);
        viewHolder.textDescription.setText(singleNews.description);

        viewHolder.buttonReadNews.setText(R.string.open_news);
        viewHolder.buttonReadNews.setTag(singleNews.id);
        viewHolder.buttonReadNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int curId = Integer.parseInt(String.valueOf(v.getTag()));
                SingleNewsModel curNews = new SingleNewsModel();
                for (SingleNewsModel item: newsList) {
                    if(item.id == curId){
                        curNews = item;
                        break;
                    }
                }
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                Intent intent = new Intent(mainCtx, SingleNewsActivity.class);
                intent.putExtra("newsData", gson.toJson(curNews));
                mainCtx.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return newsList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private TextView textTitle, textDescription, textDate, buttonReadNews;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textDate = itemView.findViewById(R.id.textDate);
            textTitle = itemView.findViewById(R.id.textTitle);
            textDescription = itemView.findViewById(R.id.textDescription);
            buttonReadNews = itemView.findViewById(R.id.buttonReadNews);
        }

    }
}
