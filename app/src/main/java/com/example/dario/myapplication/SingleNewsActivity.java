package com.example.dario.myapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dario.myapplication.models.SingleNewsModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.InputStream;
import java.net.URL;

public class SingleNewsActivity extends AppCompatActivity {

    private Bitmap bmp;
    private TextView openLink;
    private ImageView imageView, shareImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_news);

        String json = getIntent().getExtras().getString("newsData");

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        final SingleNewsModel singleNews = gson.fromJson(json, SingleNewsModel.class);

        TextView textDate = findViewById(R.id.textDate);
        textDate.setText(singleNews.annonceDate);
        TextView textTitle = findViewById(R.id.textTitle);
        textTitle.setText(singleNews.title);
        TextView testMsg = findViewById(R.id.textMsg);
        testMsg.setText(singleNews.text);

        imageView = findViewById(R.id.imageView);
        shareImage = findViewById(R.id.imageShare);
        openLink = findViewById(R.id.openLink);
    }
}
